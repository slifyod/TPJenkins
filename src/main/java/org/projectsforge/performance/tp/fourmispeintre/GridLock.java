package org.projectsforge.performance.tp.fourmispeintre;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GridLock {

    private static final int mLargeur = 2000;
    private static final int mHauteur = 2000;
    private static Lock[][] locks = new Lock[mLargeur][mHauteur];


    public static void lock(int xStart, int yStart,int xEnd, int yEnd) {
        for (int i = xStart; i < xEnd; i++) {
            if (i == mLargeur)
                i = 0;
            for (int j = yStart; j < yEnd; j++) {
                if (j == mHauteur)
                    j = 0;

                    if (locks[i][j] == null)
                        locks[i][j] = new ReentrantLock();
                    locks[i][j].lock();
                    //System.out.println("locked : " + i + " " + j);
                }

        }
    }

    public static void unlock(int xStart, int yStart,int xEnd, int yEnd) {
        for (int i = xStart; i < xEnd; i++) {
            for (int j = yStart; j < yEnd; j++) {
                if (i == mLargeur)
                    i = 0;
                if (j == mHauteur)
                    j = 0;
                if (locks[i][j] != null)
                    locks[i][j].unlock();
                locks[i][j] = null;
                //System.out.println("unlocked : " + i + " " + j);
            }
        }
    }

}
