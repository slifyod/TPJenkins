package org.projectsforge.performance.tp.fourmispeintre;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by yunus on 28/11/2016.
 */
public class AtomicTest {

    private static final int mLargeur = 2000;
    private static final int mHauteur = 2000;
    private static AtomicInteger[][] locks = new AtomicInteger[mLargeur][mHauteur];

    public static boolean isLocked(int xStart, int yStart, int xEnd, int yEnd) {
        for (int i = xStart; i < xEnd; i++) {
            if (i == mLargeur)
                i = 0;
            for (int j = yStart; j < yEnd; j++) {
                if (j == mHauteur)
                    j = 0;
                if (locks[i][j].get() == 1)
                    return true;
            }
        }
        return false;
    }

    public static void lock(int xStart, int yStart, int xEnd, int yEnd) {
        long debut = System.currentTimeMillis() % 1000;
        for (int i = xStart; i < xEnd; i++) {
            if (i == mLargeur)
                i = 0;
            for (int j = yStart; j < yEnd; j++) {
                if (j == mHauteur)
                    j = 0;
                if (locks[i][j] == null)
                    locks[i][j] = new AtomicInteger(1);
                else
                    while (isLocked(xStart+i, yStart+j, xEnd, yEnd)) {
                    if(System.currentTimeMillis() % 1000 - debut > 1000)
                        unlock(xStart,yStart,xEnd,yEnd);
                        try {
                            Thread.sleep(1000);
                            break;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                locks[i][j].getAndSet(1);

                //System.out.println("locked : " + i + " " + j);
            }
        }
    }

    public static void unlock(int xStart, int yStart, int xEnd, int yEnd) {

        for (int i = xStart; i < xEnd; i++) {
            for (int j = yStart; j < yEnd; j++) {
                if (i == mLargeur)
                    i = 0;
                if (j == mHauteur)
                    j = 0;
                locks[i][j].getAndSet(0);
                //System.out.println("unlocked : " + i + " " + j);
            }
        }
    }


}
