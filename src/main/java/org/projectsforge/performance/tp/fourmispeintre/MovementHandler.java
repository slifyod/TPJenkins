package org.projectsforge.performance.tp.fourmispeintre;

import java.util.ArrayList;

/**
 * Created by kara on 22/11/16.
 */
public class MovementHandler implements Runnable {
    private int id;
    private ArrayList<CFourmi> mColonie = new ArrayList<>();

    private PaintingAnts mApplis;

    public PaintingAnts getmApplis() {
        return mApplis;
    }

    public void setmApplis(PaintingAnts mApplis) {
        this.mApplis = mApplis;
    }

    /**
     * @param id        : i ième thread créé
     * @param pColonie  : les 20 fourmis
     * @param nbThreads : 2
     * @param pApplis
     */
    public MovementHandler(int id, ArrayList<CFourmi> pColonie, int nbThreads, PaintingAnts pApplis) {
        for (double i = 0; i < pColonie.size(); i++) {
            mColonie.add(pColonie.get((int) i));
        }
        mApplis = pApplis;
        this.id=id;
        System.out.println(id + " has " + mColonie.size());
    }

    public int getId() {
        return id;
    }

    public ArrayList<CFourmi> getmColonie() {
        return mColonie;
    }

    @Override
    public void run() {
        while (true) {
            if (!mApplis.getPause()) {
                for (int i = 0; i < mColonie.size(); i++) {
                    int oldX = mColonie.get(i).getX();
                    int oldY = mColonie.get(i).getY();

                    mColonie.get(i).deplacer();

                }
            } else {
                // try { Thread.sleep(100); } catch (InterruptedException e) { break; }
            }
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setmColonie(ArrayList<CFourmi> mColonie) {
        this.mColonie = mColonie;
    }
}
