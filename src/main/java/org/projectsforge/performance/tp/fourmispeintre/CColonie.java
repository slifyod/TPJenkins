package org.projectsforge.performance.tp.fourmispeintre;

/*
 * CColonie.java
 *
 * Created on 11 avril 2007, 16:35
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

import java.util.ArrayList;

public class CColonie implements Runnable {

    public int nbThreads =4;
    private Boolean mContinue = Boolean.TRUE;

    private PaintingAnts mApplis;

    public PaintingAnts getmApplis() {
        return mApplis;
    }

    public Boolean getmContinue() {
        return mContinue;
    }

    public void setmApplis(PaintingAnts mApplis) {
        mApplis = mApplis;
    }

    public void setmContinue(Boolean mContinue) {
        this.mContinue = mContinue;
    }

    private ArrayList<CFourmi> mColonie;

    /**
     * Creates a new instance of CColonie
     */
    public CColonie(ArrayList<CFourmi> pColonie, PaintingAnts pApplis) {
        mColonie = pColonie;
        mApplis = pApplis;
    }

    public void pleaseStop() {
        mContinue = false;
    }

    @Override
    public void run() {
        int posThread = 0;
        MovementHandler[] mhs= new MovementHandler[nbThreads];
        int sizeParts = mColonie.size()/nbThreads;
        ArrayList<CFourmi> coloni = new ArrayList<>();
        int cpt = 0;
        for(int i = 0; i< mColonie.size() ; i++)
        {
            cpt++;
            coloni.add(mColonie.get(i));
            if((cpt)==sizeParts)
            {
                mhs[posThread] = new MovementHandler(posThread, coloni, nbThreads, mApplis);
                posThread++;
                coloni.clear();
                cpt=0;
            }
        }

        Thread[] threads = new Thread[nbThreads];
        for (posThread = 0; posThread < nbThreads; posThread++) {
            threads[posThread] = new Thread(mhs[posThread]);
            threads[posThread].start();
        }

    }

}
